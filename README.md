Ansible Role: grml zsh config
=========
[![status-badge](https://ci.codeberg.org/api/badges/footur/ansible-grml-zsh-config/status.svg)](https://ci.codeberg.org/footur/ansible-grml-zsh-config)

This role installs the [grml zsh config](https://grml.org/zsh/).


Example Playbook
----------------

```yaml
- hosts: localhost
  roles:
    - ansible-grml-zsh-config
```
License
-------

CC0
